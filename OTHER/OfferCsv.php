<?php

/** Class for converting data to CSV format */
class OfferCsv {

    function generateCsv(){

        $offers = $this->offersArray();

        $this->getOffers($offers);
    }

    /**
     *
     * @param $offers
     */
    public function getOffers($offers){
        $output_filename = "offers.csv";
        $output_handle = @fopen('php://output', 'w');
        ob_clean();

        $csv_fields = array();
        $csv_fields[] = 'Offer Name';
        $csv_fields[] = 'Offer Description';

        header("Cache-Control: no-cache, must-revalidate");
        header( 'Content-Description: File Transfer' );
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename=' . $output_filename);
        header( 'Expires: 0' );
        header( 'Pragma: no-cache' );

        fputcsv($output_handle, $csv_fields);


        foreach ($offers as $offer) {

            $csv_input_array = array(
                'offer_name' => $offer['name'],
                'offer_description' => $offer['description']
            );

            fputcsv($output_handle, $csv_input_array);

        }
        fclose($output_handle);
        exit();
    }

    /**
     * Assuming data being received from a database of a large website.
     * @return array
     */
    public function offersArray(){
        $offers = array(
            'offer1' => array(
                'name' => 'Free Meals',
                'description' => 'Get free meals everyday at McDonalds'
            ),

            'offer2' => array(
                'name' => 'Holiday Trip',
                'description' => '2 day and 3 nights trip to your favourite destination in the world'
            )
        );
        return $offers;
    }

}
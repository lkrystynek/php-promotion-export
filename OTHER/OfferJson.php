<?php

/** Class for converting data to JSON format */
class OfferJson {

    public function generateJson(){
        $offers = new OfferCsv();
        ob_clean();
        header("Content-Type: application/json");

        foreach ($offers->offersArray() as $offer){
            $data = array(
                'Offer Name' => $offer['name'],
                'Offer Description' => $offer['description']
            );
            echo json_encode($data);
        }
    }

}
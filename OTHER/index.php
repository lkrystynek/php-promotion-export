<?php
require_once 'OfferCsv.php';
require_once 'OfferJson.php';
require_once 'OfferXml.php';

class Offers {
    function __construct(){
        $this->exportFormat();
    }

    public function exportFormat(){ ?>
        <html>
        <head>
            <link rel="stylesheet" type="text/css" href="css/style.css">
        </head>
        <body>
        <form method="post" action="">
            <input type="submit" name="csv" value="Get CSV" class="btn" />
            <input type="submit" name="json" value="Get JSON" class="btn_json" />
            <input type="submit" name="xml" value="Get XML" class="bx" />
        </form>
        </body>
        </html>
   <?php
        if (isset($_POST['csv'])){
       $csv = new OfferCsv();
            $csv->generateCsv();
        }
        elseif (isset($_POST['json'])){
            $json = new OfferJson();
            $json->generateJson();
        }
        elseif (isset($_POST['xml'])){
            $xml = new OfferXml();
            $xml->generateXml();
        }

    }

}

new Offers();


<?php

/**
 * Created by PhpStorm.
 * User: faisalhussain
 * Date: 31/01/2017
 * Time: 9:24 PM
 */
class OfferXml {

    public function generateXml(){
        ob_clean();
// "Create" the document.
        $xml = new DOMDocument( "1.0", "ISO-8859-15" );
        $offers = new OfferCsv();
        foreach ($offers->offersArray() as $offer){


    // Create some elements.
                $xml_offer = $xml->createElement( "Offer" );
                $xml_name = $xml->createElement( "Name", $offer['name'] );
                $xml_description = $xml->createElement( "Description", $offer['description'] );

    // Append the whole bunch.
                $xml_offer->appendChild( $xml_name );
                $xml_offer->appendChild($xml_description);


                $xml->appendChild( $xml_offer );
        }

        $xml->save("offers.xml");
        header('Content-disposition: attachment; filename="offers.xml"');
        header('Content-type: "text/xml"; charset="utf8"');
        readfile('offers.xml');
    }
}
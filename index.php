<?php

use FormatterBundle\Exception\IncorrectFormatterException;
use FormatterBundle\FeedGenerator\FormatterFactory;
use FormatterBundle\Model\Company;
use FormatterBundle\Model\Offer;
use FormatterBundle\Model\OfferFeed;
use FormatterBundle\Response;

include 'common.php';

$feed = new OfferFeed();
for ($i=1 ; $i<=20 ; $i++) {
    $offer = new Offer('Offer no. '.$i);
    $feed->addOffer($offer);
}

$formatterFactory = new FormatterFactory();

$company = new Company\CSVCompany();
$company = new Company\JSONCompany();
//$company = new Company\XMLCompany();
//$company = new Company\Company('aaa');

$response = new Response($formatterFactory, $company);

try {
    $response->sendFeed($feed);
} catch (IncorrectFormatterException $e) {
    die('Feed could not be formatted: '.$e->getMessage());
}

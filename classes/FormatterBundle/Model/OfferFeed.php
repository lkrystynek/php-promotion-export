<?php

namespace FormatterBundle\Model;



class OfferFeed
{
    /**
     * @var array
     */
    private $offers;

    /**
     * OfferFeed constructor.
     * @param array $offers
     */
    public function __construct(array $offers = array())
    {
        $this->offers = $offers;
    }

    /**
     * @return array
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param array $offers
     * @return OfferFeed
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
        return $this;
    }

    /**
     * @param Offer $offer
     * @return $this
     */
    public function addOffer(Offer $offer) {
        $this->offers[] = $offer;
        return $this;
    }


}
<?php

namespace FormatterBundle\Model;


class Offer
{
    /**
     * @var string
     */
    private $content;

    /**
     * Offer constructor.
     * @param string $content
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Offer
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }



}
<?php

namespace FormatterBundle\Model\Company;


use FormatterBundle\FeedGenerator\Feedable;

class XMLCompany extends Company
{

    /**
     * XMLCompany constructor.
     */
    public function __construct()
    {
        parent::__construct(Feedable::FORMAT_XML);
    }



}
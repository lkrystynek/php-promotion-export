<?php

namespace FormatterBundle\Model\Company;


use FormatterBundle\FeedGenerator\Feedable;

class Company implements Feedable
{
    protected $outputFormat;

    /**
     * Company constructor.
     * @param $outputFormat
     */
    public function __construct($outputFormat)
    {
        $this->outputFormat = $outputFormat;
    }

    /**
     * @return mixed
     */
    public function getOutputFormat()
    {
        return $this->outputFormat;
    }

    /**
     * @param mixed $outputFormat
     * @return Company
     */
    public function setOutputFormat($outputFormat)
    {
        $this->outputFormat = $outputFormat;
        return $this;
    }


}
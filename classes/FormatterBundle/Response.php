<?php

namespace FormatterBundle;


use FormatterBundle\Exception\IncorrectFormatterException;
use FormatterBundle\FeedGenerator\Feedable;
use FormatterBundle\FeedGenerator\FormatterFactory;
use FormatterBundle\Model\OfferFeed;

class Response
{
    /**
     * @var FormatterFactory
     */
    private $formatterFactory;

    /**
     * @var Feedable
     */
    private $company;

    /**
     * Response constructor.
     * @param FormatterFactory $formatterFactory
     * @param Feedable $company
     */
    public function __construct(FormatterFactory $formatterFactory, Feedable $company)
    {
        $this->formatterFactory = $formatterFactory;
        $this->company = $company;
    }


    /**
     * @param OfferFeed $feed
     * @throws IncorrectFormatterException
     */
    public function sendFeed(OfferFeed $feed) {
        $formatter = $this->formatterFactory->getFormatter($this->company);

        $output = $formatter->formatFeed($feed);

        header('Content-type', $formatter->getMimeType());
        echo $output;
    }
}
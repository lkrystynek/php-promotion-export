<?php

namespace FormatterBundle\FeedGenerator;


use FormatterBundle\Exception\IncorrectFormatterException;
use FormatterBundle\FeedGenerator\Formatter\CSVFormatter;
use FormatterBundle\FeedGenerator\Formatter\JSONFormatter;
use FormatterBundle\FeedGenerator\Formatter\XMLFormatter;

class FormatterFactory
{
    /**
     * @param Feedable $feedable
     * @return CSVFormatter|JSONFormatter|XMLFormatter
     * @throws IncorrectFormatterException
     */
    public function getFormatter(Feedable $feedable) {
        switch ($feedable->getOutputFormat()) {
            case Feedable::FORMAT_XML:
                return new XMLFormatter();
            case Feedable::FORMAT_CSV:
                return new CSVFormatter();
            case Feedable::FORMAT_JSON:
                return new JSONFormatter();
            default:
                throw new IncorrectFormatterException('No formatter can be found for format: '.$feedable->getOutputFormat());
        }
    }
}
<?php

namespace FormatterBundle\FeedGenerator\Formatter;


use FormatterBundle\Model\OfferFeed;

interface FormatterInterface
{
    /**
     * @param OfferFeed $feed
     * @return string
     */
    public function formatFeed(OfferFeed $feed);

    /**
     * @return string
     */
    public function getMimeType();
}
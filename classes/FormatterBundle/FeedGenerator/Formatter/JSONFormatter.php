<?php


namespace FormatterBundle\FeedGenerator\Formatter;


use FormatterBundle\Model\Offer;
use FormatterBundle\Model\OfferFeed;

class JSONFormatter implements FormatterInterface
{
    public function formatFeed(OfferFeed $feed)
    {
        $offers = array();
        /** @var Offer $offer */
        foreach ($feed->getOffers() as $offer) {
            $offers[] = array(
                'content' => $offer->getContent(),
            );
        }
        $result = array(
            'feed' => $offers,
        );
        return json_encode($result);
    }

    public function getMimeType()
    {
        return 'application/json';
    }


}
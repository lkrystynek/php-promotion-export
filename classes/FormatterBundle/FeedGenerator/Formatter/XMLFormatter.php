<?php

namespace FormatterBundle\FeedGenerator\Formatter;


use FormatterBundle\Model\Offer;
use FormatterBundle\Model\OfferFeed;

class XMLFormatter implements FormatterInterface
{
    public function formatFeed(OfferFeed $feed)
    {
        $xml = new \SimpleXMLElement('<xml />');
        $feedNode = $xml->addChild('feed');

        /** @var Offer $offer */
        foreach ($feed->getOffers() as $offer) {
            $offerNode = $feedNode->addChild('offer');
            $offerNode->addChild('content', $offer->getContent());
        }

        return $xml->asXML();
    }

    public function getMimeType()
    {
        return 'text/xml';
    }


}
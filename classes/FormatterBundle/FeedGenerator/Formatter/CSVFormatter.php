<?php

namespace FormatterBundle\FeedGenerator\Formatter;


use FormatterBundle\Model\Offer;
use FormatterBundle\Model\OfferFeed;

class CSVFormatter implements FormatterInterface
{
    public function formatFeed(OfferFeed $feed)
    {
        $csv = array(
            'Content'    // headers
        );

        /** @var Offer $offer */
        foreach ($feed->getOffers() as $offer) {
            $csv[] = $this->escapeCSVString($offer->getContent());
        }
        return join("\n", $csv);
    }

    public function getMimeType()
    {
        return 'text-csv';
    }


    private function escapeCSVString($str) {
        return '"' . str_replace('"', '""', $str) . '"';
    }

}
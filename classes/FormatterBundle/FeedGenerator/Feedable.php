<?php

namespace FormatterBundle\FeedGenerator;


interface Feedable
{
    const FORMAT_XML = 'XML';
    const FORMAT_CSV = 'CSV';
    const FORMAT_JSON = 'JSON';

    /**
     * @return string
     */
    public function getOutputFormat();
}
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('ROOT_URI', __DIR__.'/classes');
spl_autoload_register(function ($class) {
    $file = ROOT_URI.'/'. str_replace('\\', '/', $class) .'.php';
    if (file_exists($file)) {
        require $file;
    }
});